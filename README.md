# Core

<h4>Setup db</h4>

    - Change the dbname and password in db/db_schema.go.
        - Run go file to create db & tables

<h4>Run</h4>

    - Go run main.go

    - go get
        - github.com/sumohammed/middleware
        - github.com/sendgrid/sendgrid-go - github.com/sendgrid/sendgrid-go/helpers/mail
        - github.com/satori/go.uuid
        - github.com/gorilla/sessions
